import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import configureStore from "./store";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core";
import setAuthorizationToken from './utils/setAuthorizationToken';
// import * as jwt_decode from "jwt-decode";
// import { setCurrentUser } from './actions/loginAction';

const theme = createMuiTheme({
  typography: {
    useNextVariants: true
  }
});

// if (localStorage.jwtToken) {
//   setAuthorizationToken(localStorage.jwtToken);
//   configureStore.dispatch(setCurrentUser(jwt_decode(localStorage.jwtToken)));
// }

ReactDOM.render(
  <Provider store={configureStore()}>
    <MuiThemeProvider theme={theme}>
      <App />
    </MuiThemeProvider>
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
