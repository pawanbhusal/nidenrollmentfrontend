import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import Fab from '@material-ui/core/Fab';
import Icon from '@material-ui/core/Icon';

import { connect } from "react-redux";
import { fetchProvince } from "../../../actions/provinceAction";

const styles = theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing.unit * 3,
    overflowX: "auto"
  },
  table: {
    minWidth: 700
  }
});

class ProvinceList extends Component {
  componentDidMount() {
    this.props.dispatch(fetchProvince());
  }

  render() {
    const { classes,province } = this.props;
    // console.log(province);
    return (
      <div>
        <Link to="/admin/province">
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
          >
            Add
          </Button>
        </Link>
        <Paper className={classes.root}>
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                <TableCell>Province</TableCell>
                <TableCell>Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {province.items.map(row => {
                let provinceKey=row.provinceKey;
                let response = provinceKey.split("\u0000");
                let res = response[2];
                return(<TableRow key={row.provinceKey}>
                  <TableCell component="th" scope="row">
                    {row.provinceName}
                  </TableCell>
                  <TableCell>
                  <Fab color="secondary" aria-label="Edit" className={classes.fab}>
                  <Link to={{pathname:`/admin/province/edit/${res}`,data:row.provinceName}}>
                    <Icon>edit_icon</Icon>
                  </Link>
                  </Fab>
                  </TableCell>
                </TableRow>)
              })}
            </TableBody>
          </Table>
        </Paper>
      </div>
    );
  }
}

ProvinceList.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  province: state.province,
  loading: state.province.loading,
  error: state.province.error
});

export default connect(mapStateToProps)(withStyles(styles)(ProvinceList));
