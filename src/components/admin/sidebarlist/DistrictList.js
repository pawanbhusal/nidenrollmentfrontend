import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
import Fab from '@material-ui/core/Fab';
import Icon from '@material-ui/core/Icon';

import { connect } from "react-redux";
import { fetchDistrict } from "../../../actions/districtAction";

const styles = theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing.unit * 3,
    overflowX: "auto"
  },
  table: {
    minWidth: 700
  }
});

class DistrictList extends Component {
  componentDidMount() {
    this.props.dispatch(fetchDistrict());
  }
  render() {
    const { classes,district } = this.props;
    console.log(district);
    return (
      <div>
        <Link to="/admin/district">
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
          >
            Add
          </Button>
        </Link>
        <Paper className={classes.root}>
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                <TableCell>District Name</TableCell>
                <TableCell>Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {district.items.map(row => (
                <TableRow key={row.districtKey}>
                  <TableCell component="th" scope="row">
                    {row.districtName}
                  </TableCell>
                  <TableCell component="th" scope="row">
                  <Fab color="secondary" aria-label="Edit" className={classes.fab}>
                    <Icon>edit_icon</Icon>
                  </Fab>
 
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Paper>
      </div>
    );
  }
}

DistrictList.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  district: state.district,
  loading: state.district.loading,
  error: state.district.error
});

export default connect(mapStateToProps)(withStyles(styles)(DistrictList));
