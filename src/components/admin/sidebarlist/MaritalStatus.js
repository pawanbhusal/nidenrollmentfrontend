import React, { Component } from "react";

import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import axios from "axios";

import { Field, reduxForm } from "redux-form";

const styles = theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  }
});

const renderTextField = ({
  input,
  label,
  // meta: { touched, error },
  ...custom
}) => (
  <TextField
    fullWidth={true}
    label={label}
    margin="normal"
    variant="outlined"
    {...input}
    {...custom}
  />
);

export class MaritalStatus extends Component {
  submit = values => {
    const maritalStatus = {
            maritalStatus : values.maritalStatus,
  };
    console.log(maritalStatus);
    let token='eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjU2ODk2MjkzMDAsInVzZXJuYW1lIjoibWFoZXNoIiwiaWF0IjoxNTQ5NjI5MzAwfQ.di_R0MAs8lYbNATYDHzPBVssvDfqFCcdSGtef-ZUhac';
    axios
        .post("http://3.95.35.107:4000/api/maritalstatus/create", maritalStatus,
        { headers: {"Authorization" : `Bearer ${token}`} })
        .then(function(response) {
        console.log(response);
    })
        .catch(function(error) {
        console.log(error.response);
    });
    this.props.reset()
  };

  render() {
    const { classes, handleSubmit } = this.props;
    return (
      <div>
        <Grid
          container
          spacing={24}
          className=""
          style={{ margin: 0, width: "100%" }}
        >
          <form
            onSubmit={handleSubmit(this.submit)}
            className={classes.container}
            noValidate
            autoComplete="off"
          >
            <Grid item xs={12} style={{ marginLeft: "10px" }}>
              MaritalStatus
            </Grid>
            <Grid item xs={12}>
                <Field
                    name="maritalStatus"
                    component={renderTextField}
                    label="MaritalStatus"
                    className={classes.textField}
                />
            </Grid>
                <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                    className={classes.button}
                    style={{ marginLeft: "10px" }}
                >
                Submit
                </Button>
          </form>
        </Grid>
      </div>
    );
  }
}

const reduxMaritalStatus = reduxForm({
  form: "MaritalStatus"
})(withStyles(styles)(MaritalStatus));

export default reduxMaritalStatus;
