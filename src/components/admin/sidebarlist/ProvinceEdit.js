import React, { Component } from "react";

import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import axios from "axios";
import { Redirect } from "react-router-dom";

import { Field, reduxForm } from "redux-form";

const styles = theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  }
});

const renderTextField = ({
  input,
  label,
  // meta: { touched, error },
  ...custom
}) => (
  <TextField
    fullWidth={true}
    label={label}
    margin="normal"
    variant="outlined"
    {...input}
    {...custom}
  />
);

export class ProvinceEdit extends Component {
  state = {
    province:this.props.location.data
  };

  handleChange = name => event => {
      console.log(event.target.value);
    this.setState({
      [name]: event.target.value
    });
  };
  handleSubmit = event=> {
    event.preventDefault();
    // console.log()
    const province = {
      provinceName: this.state.province
    };
    console.log(province);
    let token =
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjU2OTAxNDExMzQsInVzZXJuYW1lIjoicGF3YW4iLCJpYXQiOjE1NTAxNDExMzR9.ywyKigfF5D1CRcPGhONctp8RlTanFGJsgjkuYzanH7E";
    axios
      .post(
        "http://3.95.35.107:4000/api/province-update/" +
          this.props.match.params.id,
        province,
        { headers: { Authorization: `Bearer ${token}` } }
      )
      .then(function(response) {
        console.log(response);
      })
      .catch(function(error) {
        console.log(error.response);
      });
      this.props.history.push('/admin/province-list')
    //   return <Redirect to={"/admin/province-list"} />;
  };

  render() {
    const { classes} = this.props;
    // console.log(this.props.location.data);
    return (
      <div>
        {/* {this.props.province} */}
        <Grid
          container
          spacing={24}
          className=""
          style={{ margin: 0, width: "100%" }}
        >
          <form
            onSubmit={this.handleSubmit}
            className={classes.container}
            noValidate
            autoComplete="off"
          >
            <Grid item xs={12} style={{ marginLeft: "10px" }}>
              Province
            </Grid>
            <Grid item xs={12}>
              <TextField
                id="outlined-name"
                label="Province"
                name="province"
                className={classes.textField}
                value={this.state.province}
                onChange={this.handleChange("province")}
                margin="normal"
                variant="outlined"
              />
            </Grid>
            <Button
              type="submit"
              variant="contained"
              color="primary"
              className={classes.button}
              style={{ marginLeft: "10px" }}
            >
              Submit
            </Button>
          </form>
        </Grid>
      </div>
    );
  }
}

// const mapStateToProps=(state)=>{
//   return state
// }

// const ReduxProvinceEdit = reduxForm({
//   form: "ProvinceEdit"
// })(withStyles(styles)(ProvinceEdit));

export default withStyles(styles)(ProvinceEdit);
// export default connect(mapStateToProps,actionCreators)(ReduxProvince);
