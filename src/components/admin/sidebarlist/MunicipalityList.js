import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import Fab from "@material-ui/core/Fab";
import Icon from "@material-ui/core/Icon";

import { connect } from "react-redux";
import { fetchMunicipality } from "../../../actions/municipalityAction";

const styles = theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing.unit * 3,
    overflowX: "auto"
  },
  table: {
    minWidth: 700
  }
});

class MunicipalityList extends Component {
  componentDidMount() {
    this.props.dispatch(fetchMunicipality());
  }
  render() {
    const { classes, municipality } = this.props;
    console.log(municipality);
    return (
      <div>
        <Link to="/admin/municipality">
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
          >
            Add
          </Button>
        </Link>
        <Paper className={classes.root}>
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                <TableCell>Municipality Name</TableCell>
                <TableCell align="right">Municipality Type</TableCell>
                <TableCell align="right">Wards</TableCell>
                <TableCell align="right">Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {municipality.items.map(row => (
                <TableRow key={row.municipalityKey}>
                  <TableCell component="th" scope="row">
                    {row.municipalityName}
                  </TableCell>
                  <TableCell align="right">{row.municipalityType}</TableCell>
                  <TableCell align="right">{row.totalWards}</TableCell>
                  <TableCell component="th" scope="row">
                    <Fab
                      color="secondary"
                      aria-label="Edit"
                      className={classes.fab}
                    >
                      <Icon>edit_icon</Icon>
                    </Fab>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Paper>
      </div>
    );
  }
}

MunicipalityList.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  municipality: state.municipality,
  loading: state.municipality.loading,
  error: state.municipality.error
});

export default connect(mapStateToProps)(withStyles(styles)(MunicipalityList));
