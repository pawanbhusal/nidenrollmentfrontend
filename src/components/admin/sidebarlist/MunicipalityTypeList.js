import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Fab from "@material-ui/core/Fab";
import Icon from "@material-ui/core/Icon";

import axios from "axios";

const styles = theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing.unit * 3,
    overflowX: "auto"
  },
  table: {
    minWidth: 700
  }
});

class MunicipalityTypeList extends Component {
  state = {
    municipalityTypeList: []
  };

  componentDidMount() {
    let token =
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjU2ODk2MjkzMDAsInVzZXJuYW1lIjoibWFoZXNoIiwiaWF0IjoxNTQ5NjI5MzAwfQ.di_R0MAs8lYbNATYDHzPBVssvDfqFCcdSGtef-ZUhac";
    axios
      .get("http://3.95.35.107:4000/api/municipalitytype-list", {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(res => {
        const municipalityTypeList = res.data.message;
        this.setState({ municipalityTypeList });
      });
  }

  render() {
    const { classes } = this.props;

    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell>Municipality Type</TableCell>
              <TableCell>Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.state.municipalityTypeList.map(row => (
              <TableRow key={row.municipalityKey}>
                <TableCell component="th" scope="row">
                  {row.municipalityType}
                </TableCell>
                <TableCell>
                  <Fab
                    color="secondary"
                    aria-label="Edit"
                    className={classes.fab}
                  >
                    <Icon>edit_icon</Icon>
                  </Fab>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

MunicipalityTypeList.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(MunicipalityTypeList);
