import React, { Component } from "react";

import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import axios from "axios";

import { Field, reduxForm } from "redux-form";
// import { connect } from "react-redux";

// import {fetchDistrict} from '../../../actions/districtAction';

const styles = theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  }
});

const renderTextField = ({
  input,
  label,
  // meta: { touched, error },
  ...custom
}) => (
  <TextField
    fullWidth={true}
    label={label}
    margin="normal"
    variant="outlined"
    {...input}
    {...custom}
  />
);

export class MunicipalityType extends Component {

  componentDidMount() {
    // this.props.dispatch(fetchDistrict());
  }

  submit = values => {
    const municipalityType = {
            municipalityType : values.municipalityType,
    };
    console.log(municipalityType);
    let token='eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjU2ODk2MjkzMDAsInVzZXJuYW1lIjoibWFoZXNoIiwiaWF0IjoxNTQ5NjI5MzAwfQ.di_R0MAs8lYbNATYDHzPBVssvDfqFCcdSGtef-ZUhac';
    axios
        .post("http://3.95.35.107:4000/api/municipalitytype/create", 
        municipalityType,
        { headers: {"Authorization" : `Bearer ${token}`} })
        .then(function(response) {
        console.log(response);
    })
        .catch(function(error) {
        console.log(error.response);
    });
    this.props.reset()
  };

  render() {
    const { classes, handleSubmit } = this.props;
    // console.log('Propsss', this.props.district);
    return (
      <div>
        <Grid
          container
          spacing={24}
          className=""
          style={{ margin: 0, width: "100%" }}
        >
          <form
            onSubmit={handleSubmit(this.submit)}
            className={classes.container}
            noValidate
            autoComplete="off"
          >
            <Grid item xs={12} style={{ marginLeft: "10px" }}>
              MunicipalityType
            </Grid>
            <Grid item xs={12}>
                <Field
                    name="municipalityType"
                    component={renderTextField}
                    label="Municipality Type"
                    className={classes.textField}
                />
            </Grid>
                <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                    className={classes.button}
                    style={{ marginLeft: "10px" }}
                >
                Submit
                </Button>
          </form>
        </Grid>
      </div>
    );
  }
}

const reduxMunicipalityType = reduxForm({
  form: "MunicipalityType"
})(withStyles(styles)(MunicipalityType));

// export default connect(mapStateToProps)(reduxMunicipalityType);
export default reduxMunicipalityType;
