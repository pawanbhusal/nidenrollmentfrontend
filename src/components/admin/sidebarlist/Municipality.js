import React, { Component } from "react";

import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import axios from "axios";

import { Field, reduxForm } from "redux-form";
// import { connect } from "react-redux";

// import {fetchDistrict} from '../../../actions/districtAction';

const styles = theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  }
});

const renderTextField = ({
  input,
  label,
  // meta: { touched, error },
  ...custom
}) => (
  <TextField
    // fullWidth={true}
    label={label}
    margin="normal"
    variant="outlined"
    {...input}
    {...custom}
  />
);

const renderSelectField = ({
  input,
  label,
  // meta: { touched, error },
  children,
  ...custom
}) => (
  <TextField
    // id="outlined-select-currency-native"
    label={label}
    margin="normal"
    variant="outlined"
    //   errorText={touched && error}
    {...input}
    // onChange={(event, index, value) => input.onChange(value)}
    children={children}
    {...custom}
  />
);

class Municipality extends Component {
  state = {
    addressList: [],
    selDistrict: [],
    municipalityType:[]
  };

  componentDidMount() {
    let token =
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjU2ODk2MjkzMDAsInVzZXJuYW1lIjoibWFoZXNoIiwiaWF0IjoxNTQ5NjI5MzAwfQ.di_R0MAs8lYbNATYDHzPBVssvDfqFCcdSGtef-ZUhac";
    axios
      .get("http://3.95.35.107:4000/api/address-list", {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(res => {
        const addressList = res.data.message;
        // console.log(addressList);
        this.setState({ addressList });
      });

      axios
      .get("http://3.95.35.107:4000/api/municipalitytype-list", {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(res => {
        const municipalityType = res.data.message;
        // console.log(addressList);
        this.setState({ municipalityType });
      });
  }

  submit = values => {
    const municipality = {
      municipalityName: values.municipalityName,
      totalWards: parseInt(values.totalWards),
      municipalityType: values.municipalityType,
      districtKey: values.districtKey
    };
    console.log(municipality);
    let token =
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjU2ODk2MjkzMDAsInVzZXJuYW1lIjoibWFoZXNoIiwiaWF0IjoxNTQ5NjI5MzAwfQ.di_R0MAs8lYbNATYDHzPBVssvDfqFCcdSGtef-ZUhac";
    axios
      .post("http://3.95.35.107:4000/api/municipality/create", municipality, {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(function(response) {
        console.log(response);
      })
      .catch(function(error) {
        console.log(error.response);
      });
    this.props.reset();
  };

  provinceClick=(e)=>{
    let selProvince = this.state.addressList.filter(c => c.provinceKey === e.target.value);
    let selDistrict=selProvince[0].districts;
    // console.log(selDistrict)
    this.setState({selDistrict});
  }

  districtClick=(e)=>{
    let seldistrict = this.state.selDistrict.filter(c => c.districtKey === e.target.value);
    let selMun=seldistrict[0].municipalities;
    console.log(selMun)
    this.setState({selMun});
  }

  render() {
    const { classes, handleSubmit, district } = this.props;
    // console.log('Propsss', this.props.district);
    return (
      <div>
        <Grid
          container
          spacing={24}
          className=""
          style={{ margin: 0, width: "100%" }}
        >
          <form
            onSubmit={handleSubmit(this.submit)}
            className={classes.container}
            noValidate
            autoComplete="off"
          >
            <Grid item xs={12} style={{ marginLeft: "10px" }}>
              Municipality
            </Grid>
            <Grid item xs={12}>
              <Field
                name="provinceKey"
                select
                component={renderSelectField}
                label="Select Province"
                onChange={this.provinceClick}
                style={{ width: "18%" }}
                SelectProps={{
                  native: true,
                  autoWidth: true,
                  MenuProps: {
                    className: classes.menu
                  }
                }}
              >
                <option />
                {this.state.addressList.map(option => (
                  <option key={option.provinceKey} value={option.provinceKey}>
                    {option.provinceName}
                  </option>
                ))}
              </Field>
              <Field
                name="districtKey"
                select
                component={renderSelectField}
                label="Select District"
                style={{ width: "18%" }}
                onChange={this.districtClick}
                SelectProps={{
                  native: true,
                  autoWidth: true,
                  MenuProps: {
                    className: classes.menu
                  }
                }}
              >
                <option />
                {this.state.selDistrict.map(option => (
                  <option key={option.districtKey} value={option.districtKey}>
                    {option.districtName}
                  </option>
                ))}
              </Field>
              <Field
              name="municipalityType"
              select
              component={renderSelectField}
              label="Select Municipality Type"
              style={{ width: "18%" }}
              SelectProps={{
                native: true,
                autoWidth: true,
                MenuProps: {
                  className: classes.menu
                }
              }}
            >
              <option />
              {this.state.municipalityType.map(option => (
                <option key={option.municipalityKey} value={option.municipalityType}>
                  {option.municipalityType}
                </option>
              ))}
            </Field>
              <Field
                name="municipalityName"
                component={renderTextField}
                label="Municipality Name"
                className={classes.textField}
              />
              <Field
                name="totalWards"
                component={renderTextField}
                label="Total Wards"
                className={classes.textField}
              />
            </Grid>
            <Button
              type="submit"
              variant="contained"
              color="primary"
              className={classes.button}
              style={{ marginLeft: "10px" }}
            >
              Submit
            </Button>
          </form>
        </Grid>
      </div>
    );
  }
}

// const mapStateToProps = state => ({
//   district: state.district,
//   loading: state.district.loading,
//   error: state.district.error
// });

const reduxMunicipality = reduxForm({
  form: "Municipality"
})(withStyles(styles)(Municipality));

export default reduxMunicipality;
// export default connect(mapStateToProps)(reduxMunicipality);
