import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Fab from "@material-ui/core/Fab";
import Icon from "@material-ui/core/Icon";

import axios from "axios";

const styles = theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing.unit * 3,
    overflowX: "auto"
  },
  table: {
    minWidth: 700
  }
});

class MaritalStatusList extends Component {
  state = {
    maritalStatusList: []
  };

  componentDidMount() {
    let token =
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjU2ODk2MjkzMDAsInVzZXJuYW1lIjoibWFoZXNoIiwiaWF0IjoxNTQ5NjI5MzAwfQ.di_R0MAs8lYbNATYDHzPBVssvDfqFCcdSGtef-ZUhac";
    axios
      .get("http://3.95.35.107:4000/api/maritalstatus-list", {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(res => {
        const maritalStatusList = res.data.message;
        this.setState({ maritalStatusList });
      });
  }

  render() {
    const { classes } = this.props;
    console.log(this.state.maritalStatusList);
    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell>Marital Status List</TableCell>
              <TableCell>Action</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.state.maritalStatusList.map(row => (
              <TableRow key={row.maritalKey}>
                <TableCell component="th" scope="row">
                  {row.maritalStatus}
                </TableCell>
                <TableCell>
                  <Fab
                      color="secondary"
                      aria-label="Edit"
                      className={classes.fab}
                    >
                      <Icon>edit_icon</Icon>
                  </Fab>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

MaritalStatusList.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(MaritalStatusList);
