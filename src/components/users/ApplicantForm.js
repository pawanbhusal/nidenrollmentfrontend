import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import { Field, reduxForm } from "redux-form";
// import { connect } from "react-redux";
import axios from "axios";

// import { fetchDistrict } from "../../actions/districtAction";
// import { fetchProvince } from "../../actions/provinceAction";
// import { fetchMunicipality } from "../../actions/municipalityAction";
// import { simpleAction } from '../../actions/index';

const styles = theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  }
});

const currencies = [
  {
    label: "test"
  },
  {
    label: "€sdsad"
  },
  {
    label: "฿sadsad"
  },
  {
    label: "¥asdsfdsf"
  }
];

const renderTextField = ({
  input,
  label,
  // meta: { touched, error },
  ...custom
}) => (
  <TextField
    //   fullWidth={true}
    label={label}
    margin="normal"
    variant="outlined"
    {...input}
    {...custom}
  />
);

const renderSelectField = ({
  input,
  label,
  // meta: { touched, error },
  children,
  ...custom
}) => (
  <TextField
    // id="outlined-select-currency-native"
    label={label}
    margin="normal"
    variant="outlined"
    //   errorText={touched && error}
    {...input}
    // onChange={(event, index, value) => input.onChange(value)}
    children={children}
    {...custom}
  />
);

export class ApplicantForm extends Component {
  //   componentDidMount() {
  //     this.props.fetchDistrict();
  //     this.props.fetchProvince();
  //     this.props.fetchMunicipality();
  //   }
  state = {
    addressList:[],
    sexlist:[],
    selDistrict:[],
    selMun:[]
  };
  componentDidMount() {
    let token =
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjU2ODk2MjkzMDAsInVzZXJuYW1lIjoibWFoZXNoIiwiaWF0IjoxNTQ5NjI5MzAwfQ.di_R0MAs8lYbNATYDHzPBVssvDfqFCcdSGtef-ZUhac";
    axios
      .get("http://3.95.35.107:4000/api/address-list", {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(res => {
        const addressList = res.data.message;
        // console.log(addressList);
        this.setState({ addressList });
      });

    axios
      .get("http://3.95.35.107:4000/api/sex-list", {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(res => {
        const sexlist = res.data.message;
        // console.log(addressList);
        this.setState({ sexlist });
      });
    // .catch(function(error) {
    // console.log(error.response);
    // });
  }

  submit = values => {
    console.log(values);
    const applicantForm = {
      applicantName: {
        firstName: "Kailash",
        middleName: "Sharan",
        lastName: "Baral"
      },
      dateOfBirthBS: "2052-09-15T14:00:12+05:45",
      citizenshipType: "Bansaj",
      sex: "Male",
      maritalStatus: "Unmarried",
      permanentAddress: {
        provinceKey: values.provinceKey,
        districtKey: values.districtKey,
        municipalityKey: values.municipalityKey,
        wardNumber: 12
      }
    };
    console.log(applicantForm);
    let token =
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjU2ODk2MjkzMDAsInVzZXJuYW1lIjoibWFoZXNoIiwiaWF0IjoxNTQ5NjI5MzAwfQ.di_R0MAs8lYbNATYDHzPBVssvDfqFCcdSGtef-ZUhac";
    axios
      .post("http://3.95.35.107:4000/api/applicantform/create", applicantForm, {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(function(response) {
        console.log(response);
      })
      .catch(function(error) {
        console.log(error.response);
      });
    this.props.reset();
  };

  provinceClick=(e)=>{
    let selProvince = this.state.addressList.filter(c => c.provinceKey === e.target.value);
    let selDistrict=selProvince[0].districts;
    // console.log(selDistrict)
    this.setState({selDistrict});
  }

  districtClick=(e)=>{
    let seldistrict = this.state.selDistrict.filter(c => c.districtKey === e.target.value);
    let selMun=seldistrict[0].municipalities;
    console.log(selMun)
    this.setState({selMun});
  }

  render() {
    const {
      classes,
      handleSubmit
      //   district,
      //   province,
      //   municipality
    } = this.props;
    console.log(this.state.addressList)
    return (
      <Grid
        container
        spacing={24}
        className="applicant-form"
        style={{ margin: 0, width: "100%" }}
      >
        <form
          onSubmit={handleSubmit(this.submit)}
          className={classes.container}
          noValidate
          autoComplete="off"
        >
          {/* <Grid item xs={12} style={{marginLeft: '10px'}}>National Identity Number</Grid> */}
          <Grid item xs={6}>
            {/* <Field
                name="nationalIdentityNumber"
                component={renderTextField}
                label="National Identity Number"
                className={classes.textField}
            /> */}
          </Grid>
          <Grid item xs={12} style={{ marginLeft: "10px" }}>
            Applicant Full Name
          </Grid>
          <Grid item xs={12}>
            <Field
              name="firstName"
              component={renderTextField}
              label="First Name"
              className={classes.textField}
            />
            <Field
              name="middleName"
              component={renderTextField}
              label="Middle Name"
              className={classes.textField}
            />
            <Field
              name="lastName"
              component={renderTextField}
              label="Last Name"
              className={classes.textField}
            />
          </Grid>
          <Grid item xs={12}>
            <Field
             name="sex"
             select
             component={renderSelectField}
             label="Select Sex"
             style={{ width: "18%" }}
             SelectProps={{
               native: true,
               autoWidth: true,
               MenuProps: {
                 className: classes.menu
               }
             }}
           >
             <option />
             {this.state.sexlist.map(option => (
               <option key={option.sexKey} value={option.sexKey}>
                 {option.sex}
               </option>
             ))}
           </Field>
            <Field
              name="maritalStatus"
              component={renderTextField}
              label="Marital Status"
              className={classes.textField}
            />
          </Grid>
          <Grid item xs={12} style={{ marginLeft: "10px" }}>
            Address
          </Grid>
          <Grid item xs={12}>
            <Field
              name="provinceKey"
              select
              component={renderSelectField}
              label="Select Province"
              onChange={this.provinceClick}
              style={{ width: "18%" }}
              SelectProps={{
                native: true,
                autoWidth: true,
                MenuProps: {
                  className: classes.menu
                }
              }}
            >
              <option />
              {this.state.addressList.map(option => (
                <option key={option.provinceKey} value={option.provinceKey}>
                  {option.provinceName}
                </option>
              ))}
            </Field>
            <Field
              name="districtKey"
              select
              component={renderSelectField}
              label="Select District"
              style={{ width: "18%" }}
              onChange={this.districtClick}
              SelectProps={{
                native: true,
                autoWidth: true,
                MenuProps: {
                  className: classes.menu
                }
              }}
            >
              <option />
              {this.state.selDistrict.map(option => (
                <option key={option.districtKey} value={option.districtKey}>
                  {option.districtName}
                </option>
              ))}
            </Field>
            <Field
              name="municipalityKey"
              select
              component={renderSelectField}
              label="Select Municipality"
              style={{ width: "18%" }}
              SelectProps={{
                native: true,
                autoWidth: true,
                MenuProps: {
                  className: classes.menu
                }
              }}
            >
              <option />
              {this.state.selMun.map(option => (
                <option key={option.municipalityKey} value={option.municipalityKey}>
                  {option.municipalityName}
                </option>
              ))}
            </Field>
            <Field
              name="wardNumber"
              component={renderTextField}
              label="Ward Number"
              className={classes.textField}
            />
          </Grid>
          <Button
            type="submit"
            variant="contained"
            color="primary"
            className={classes.button}
            style={{ marginLeft: "10px" }}
          >
            Submit
          </Button>
        </form>
      </Grid>
    );
  }
}

//const mapStateToProps = state => ({
//municipality: state.municipality,
// loadingm: state.municipality.loading,
// errorsm: state.municipality.error,

//province: state.province,
// loadings: state.province.loading,
// errors: state.province.error,

//district: state.district
// loading: state.district.loading,
// error: state.district.error
//});

// const mapDispatchToProps = dispatch => ({
//   fetchDistrict: () => dispatch(fetchDistrict()),
//   fetchProvince: () => dispatch(fetchProvince()),
//   fetchMunicipality: () => dispatch(fetchMunicipality())
// });

const reduxApplicantForm = reduxForm({
  form: "ApplicantForm"
})(withStyles(styles)(ApplicantForm));

export default reduxApplicantForm;

// export default connect(
//     mapStateToProps,
//     mapDispatchToProps
//   )(reduxApplicantForm);
