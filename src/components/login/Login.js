import React,{Component} from "react";
import PropTypes from "prop-types";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import FormControl from "@material-ui/core/FormControl";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";
import {Redirect} from 'react-router-dom';
// import { connect } from 'react-redux';
// import { login } from '../../actions/loginAction';
import {loginData} from './LoginApi';

const styles = theme => ({
  main: {
    width: "auto",
    display: "block", // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: "auto",
      marginRight: "auto"
    }
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
      .spacing.unit * 3}px`
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing.unit
  },
  submit: {
    marginTop: theme.spacing.unit * 3
  }
});

class Login extends Component {
  state = {
    username: "",
    password: "",
    redirectToReferrer: false
  };

  submit=(e)=>{
    e.preventDefault();
    if(this.state.username && this.state.password){
      loginData('login',this.state).then((result) => {
        let responseJson = result;
        console.log(responseJson);
        if(responseJson){         
          sessionStorage.setItem('userData',JSON.stringify(responseJson));
          this.setState({redirectToReferrer: true});
        }
       });
    }
    // this.props.login(this.state).then(
    //     (res) => this.props.history.push('/admin')
        // (err) => this.setState({ errors: err.response.data.errors, isLoading: false })
    // );
  }

  onChange=(e)=> {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    const { classes } = this.props;
    const { username, password} = this.state;
    if (this.state.redirectToReferrer) {
      return (<Redirect to={'/admin/province'}/>)
    }
   
    if(sessionStorage.getItem('userData')){
      return (<Redirect to={'/admin/province'}/>)
    }
    return (
      <main className={classes.main} onSubmit={this.submit}>
        <CssBaseline />
        <Paper className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <form className={classes.form}>
            <FormControl margin="normal" required fullWidth>
              <InputLabel htmlFor="username">User Name</InputLabel>
              <Input id="username" 
                name="username" 
                autoComplete="username" 
                onChange={this.onChange}
                value={username}
                autoFocus 
               />
            </FormControl>
            <FormControl margin="normal" required fullWidth>
              <InputLabel htmlFor="password">Password</InputLabel>
              <Input
                name="password"
                type="password"
                id="password"
                onChange={this.onChange}
                value={password}
                autoComplete="current-password"
              />
            </FormControl>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Sign in
            </Button>
          </form>
        </Paper>
      </main>
    );
  }
}

Login.propTypes = {
  classes: PropTypes.object.isRequired
};

export default (withStyles(styles)(Login));
// export default connect(null, { login })(withStyles(styles)(Login));
