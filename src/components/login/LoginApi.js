export function loginData(type, userData) {
    let BaseURL = 'http://3.95.35.107:4000/';
    //let BaseURL = 'http://localhost/PHP-Slim-Restful/api/';

    const options = { 
        method: 'post',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
           body: JSON.stringify(userData)
      
      }   

    return new Promise((resolve, reject) =>{
    
         
        fetch(BaseURL+type, options)
          .then((response) => response.json())
          .then((res) => {
            resolve(res);
          })
          .catch((error) => {
            reject(error);
          });

  
      });
}