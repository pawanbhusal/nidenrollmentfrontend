import axios from "axios";


// Url
// You can use it in many places
// So better keep it in env files || constants

// More better headers --> library 
export function getProvince() {
  let token =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjU2ODk2MjkzMDAsInVzZXJuYW1lIjoibWFoZXNoIiwiaWF0IjoxNTQ5NjI5MzAwfQ.di_R0MAs8lYbNATYDHzPBVssvDfqFCcdSGtef-ZUhac";
  return axios
    .get("http://3.95.35.107:4000/api/province-list", {
      headers: { Authorization: `Bearer ${token}` }
    })
    .then(res => {
      // console.log('province',res);
      return res;
    });
}

export function fetchProvince() {
  return dispatch => {
    dispatch(fetchProvinceBegin());
    return getProvince()
      .then(res => {
        dispatch(fetchProvinceSuccess(res));
        return res;
      })
      .catch(error => dispatch(fetchProvinceFailure(error)));
  };
}

export const FETCH_PROVINCE_BEGIN = "FETCH_PROVINCE_BEGIN";
export const FETCH_PROVINCE_SUCCESS = "FETCH_PROVINCE_SUCCESS";
export const FETCH_PROVINCE_FAILURE = "FETCH_PROVINCE_FAILURE";

export const fetchProvinceBegin = () => ({
  type: FETCH_PROVINCE_BEGIN
});

export const fetchProvinceSuccess = district => ({
  type: FETCH_PROVINCE_SUCCESS,
  payload: district
});

export const fetchProvinceFailure = error => ({
  type: FETCH_PROVINCE_FAILURE,
  payload: {error}
});
