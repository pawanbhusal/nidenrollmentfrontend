import axios from "axios";

export function getDistrict() {
  let token =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjU2ODk2MjkzMDAsInVzZXJuYW1lIjoibWFoZXNoIiwiaWF0IjoxNTQ5NjI5MzAwfQ.di_R0MAs8lYbNATYDHzPBVssvDfqFCcdSGtef-ZUhac";
  return axios
    .get("http://3.95.35.107:4000/api/district-list", {
      headers: { Authorization: `Bearer ${token}` }
    })
    .then(res => {
      console.log('district',res);
      return res;
    });
}

export function fetchDistrict() {
  return dispatch => {
    dispatch(fetchDistrictBegin());
    return getDistrict()
      .then(res => {
        dispatch(fetchDistrictSuccess(res));
        return res;
      })
      .catch(error => dispatch(fetchDistrictFailure(error)));
  };
}

export const FETCH_DISTRICT_BEGIN = "FETCH_DISTRICT_BEGIN";
export const FETCH_DISTRICT_SUCCESS = "FETCH_DISTRICT_SUCCESS";
export const FETCH_DISTRICT_FAILURE = "FETCH_DISTRICT_FAILURE";

export const fetchDistrictBegin = () => ({
  type: FETCH_DISTRICT_BEGIN
});

export const fetchDistrictSuccess = district => ({
  type: FETCH_DISTRICT_SUCCESS,
  payload: district
});

export const fetchDistrictFailure = error => ({
  type: FETCH_DISTRICT_FAILURE,
  payload: error
});
