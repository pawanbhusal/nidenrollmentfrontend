import axios from "axios";
import setAuthorizationToken from "../utils/setAuthorizationToken";
import * as jwt_decode from "jwt-decode";
// import { SET_CURRENT_USER } from './types';
export const SET_CURRENT_USER = "SET_CURRENT_USER";

export function setCurrentUser(user) {
  return {
    type: SET_CURRENT_USER,
    user
  };
}

export function login(data) {
  return dispatch => {
    return axios.post("http://3.95.35.107:4000/login", data).then(res => {
      console.log(res);
      const token = res.data.token;
      localStorage.setItem("jwtToken", token);
      setAuthorizationToken(token);
    //   console.log(jwt_decode(token));
      dispatch(setCurrentUser(jwt_decode(token)));
    });
  };
}
