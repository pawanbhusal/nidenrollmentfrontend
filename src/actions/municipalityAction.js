import axios from "axios";


// Url
// You can use it in many places
// So better keep it in env files || constants

// More better headers --> library 
export function getMunicipality() {
  let token =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjU2ODk2MjkzMDAsInVzZXJuYW1lIjoibWFoZXNoIiwiaWF0IjoxNTQ5NjI5MzAwfQ.di_R0MAs8lYbNATYDHzPBVssvDfqFCcdSGtef-ZUhac";
  return axios
    .get("http://3.95.35.107:4000/api/municipality-list", {
      headers: { Authorization: `Bearer ${token}` }
    })
    .then(res => {
        console.log('mun',res)
      return res;
    });
}

export function fetchMunicipality() {
  return dispatch => {
    dispatch(fetchMunicipalityBegin());
    return getMunicipality()
      .then(res => {
        dispatch(fetchMunicipalitySuccess(res));
        return res;
      })
      .catch(error => dispatch(fetchMunicipalityFailure(error)));
  };
}

export const FETCH_MUNICIPALITY_BEGIN = "FETCH_MUNICIPALITY_BEGIN";
export const FETCH_MUNICIPALITY_SUCCESS = "FETCH_MUNICIPALITY_SUCCESS";
export const FETCH_MUNICIPALITY_FAILURE = "FETCH_MUNICIPALITY_FAILURE";

export const fetchMunicipalityBegin = () => ({
  type: FETCH_MUNICIPALITY_BEGIN
});

export const fetchMunicipalitySuccess = municipality => ({
  type: FETCH_MUNICIPALITY_SUCCESS,
  payload: municipality
});

export const fetchMunicipalityFailure = error => ({
  type: FETCH_MUNICIPALITY_FAILURE,
  payload: error
});
