import axios from "axios";

export function loadProvince(){
    return(dispatch)=>{
        return axios.get("http://3.95.35.107:4000/api/province_list").then((response)=>{
            dispatch(getProvince(response));
        })
    }
}

export function getProvince(province) {
    return{
        type:"GET_PROVINCE",
        payload:province
    }
}

export const simpleAction = () => dispatch => {
    dispatch({
     type: 'SIMPLE_ACTION',
     payload: 'result_of_simple_action'
    })
}