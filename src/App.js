import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Route,
    Switch,
    Redirect
} from 'react-router-dom';
import ApplicantForm from './components/users/ApplicantForm';
import Province from './components/admin/sidebarlist/Province';
import District from './components/admin/sidebarlist/District';
import MaritalStatus from './components/admin/sidebarlist/MaritalStatus';
import Municipality from './components/admin/sidebarlist/Municipality';
import Sex from './components/admin/sidebarlist/Sex';
import DistrictList from './components/admin/sidebarlist/DistrictList';
import ProvinceList from './components/admin/sidebarlist/ProvinceList';
import MunicipalityList from './components/admin/sidebarlist/MunicipalityList';
import SexList from './components/admin/sidebarlist/SexList';
import MunicipalityTypeList from './components/admin/sidebarlist/MunicipalityTypeList';
import MaritalStatusList from './components/admin/sidebarlist/MaritalStatusList';
import ProvinceEdit from './components/admin/sidebarlist/ProvinceEdit';


import Menu from './components/admin/Menu';
import MunicipalityType from './components/admin/sidebarlist/MunicipalityType';
import Login from './components/login/Login';



class App extends Component {
    render() {
        return (
            <Router>
                <Switch>
                    <Route exact path="/" component={Login} />
                    <Route path="/admin">
                    <Menu>
                        <Switch>
                            <Route exact path="/admin/province" component={Province} />
                            <Route exact path="/admin/province/edit/:id" component={ProvinceEdit} />
                            <Route path="/admin/district" component={District} />
                            <Route path="/admin/marital-status" component={MaritalStatus} />
                            <Route path="/admin/municipality-type" component={MunicipalityType} />
                            <Route path="/admin/municipality" component={Municipality} />
                            <Route path="/admin/sex" component={Sex} />
                            <Route path="/admin/district-list" component={DistrictList} />
                            <Route path="/admin/province-list" component={ProvinceList} />
                            <Route path="/admin/municipality-list" component={MunicipalityList} />
                            <Route path="/admin/municipality-type-list" component={MunicipalityTypeList} />
                            <Route path="/admin/sex-list" component={SexList} />
                            <Route path="/admin/marital-status-list" component={MaritalStatusList} />
                            <Route exact path="/admin/user" component={ApplicantForm} />
                            <Redirect to="/admin/province" />    
                        </Switch>
                    </Menu>
                    </Route>
                </Switch>
            </Router>
        );
    }
}

export default App;
