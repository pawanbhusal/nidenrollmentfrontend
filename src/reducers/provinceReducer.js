import {
    FETCH_PROVINCE_BEGIN,
    FETCH_PROVINCE_SUCCESS,
    FETCH_PROVINCE_FAILURE
  } from '../actions/provinceAction';
  
  const initialState = {
    items: [],
    loading: false,
    error: null
  };
  
  export default function provinceReducer(
    state = initialState,
    action
  ) {
    switch (action.type) {
      case FETCH_PROVINCE_BEGIN:
        return {
          ...state,
          loading: true,
          error: null
        };
  
      case FETCH_PROVINCE_SUCCESS:
    //   console.log(action)
        return {
          ...state,
          loading: false,
          items: action.payload.data.message
        };
  
      case FETCH_PROVINCE_FAILURE:
        return {
          ...state,
          loading: false,
          error: action.payload.error,
          items: []
        };
  
      default:
        return state;
    }
}
  