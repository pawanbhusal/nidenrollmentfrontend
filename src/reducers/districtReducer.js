import {
    FETCH_DISTRICT_BEGIN,
    FETCH_DISTRICT_SUCCESS,
    FETCH_DISTRICT_FAILURE
  } from '../actions/districtAction';
  
  const initialState = {
    items: [],
    loading: false,
    error: null
  };
  
  export default function districtReducer(
    state = initialState,
    action
  ) {
    switch (action.type) {
      case FETCH_DISTRICT_BEGIN:
        return {
          ...state,
          loading: true,
          error: null
        };
  
      case FETCH_DISTRICT_SUCCESS:
        return {
          ...state,
          loading: false,
          items: action.payload.data.message
        };
  
      case FETCH_DISTRICT_FAILURE:
        return {
          ...state,
          loading: false,
          error: action.payload.error,
          items: []
        };
  
      default:
        return state;
    }
}
  