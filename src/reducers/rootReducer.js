import { combineReducers } from 'redux';
import {reducer as formReducer} from 'redux-form';
import users from './usersReducer';
import district from './districtReducer';
import province from './provinceReducer';
import municipality from './municipalityReducer';
import login from './loginReducer';

export default combineReducers({
    login,
    municipality,
    province,
    district,
    users,
    form: formReducer
});