import {
    FETCH_MUNICIPALITY_BEGIN,
    FETCH_MUNICIPALITY_SUCCESS,
    FETCH_MUNICIPALITY_FAILURE
  } from '../actions/municipalityAction';
  
  const initialState = {
    items: [],
    loading: false,
    error: null
  };
  
  export default function municipalityReducer(
    state = initialState,
    action
  ) {
    switch (action.type) {
      case FETCH_MUNICIPALITY_BEGIN:
        return {
          ...state,
          loading: true,
          error: null
        };
  
      case FETCH_MUNICIPALITY_SUCCESS:
    //   console.log(action)
        return {
          ...state,
          loading: false,
          items: action.payload.data.message
        };
  
      case FETCH_MUNICIPALITY_FAILURE:
        return {
          ...state,
          loading: false,
          error: action.payload.error,
          items: []
        };
  
      default:
        return state;
    }
}
  